package br.com.algaworks.ecommerce.model;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SecondaryTable(name = "cliente_detalhe", 
			    pkJoinColumns = @PrimaryKeyJoinColumn(name = "cliente_id"),
			    foreignKey = @ForeignKey(name = "fk_cliente_detalhe_cliente"))
@Entity
@Table(name = "cliente", 
	   uniqueConstraints = { @UniqueConstraint(name = "unq_cpf", columnNames = {"cpf"}) }, // CPF �nico e cpf � o nome da coluna no banco de dados, n�o do atributo
	   indexes = { @Index(name = "idx_nome", columnList = "nome") }) // Index facilita a busca de dados na base, aqui nome � o nome da coluna no banco de dados, n�o do atributo
public class Cliente extends EntidadeBaseInteger {

	@Column(length = 100, nullable = false)
	private String nome;
	
	@Column(length = 14, nullable = false)
	private String cpf;

	@Transient // Com essa anota��o o atributo ser� ignorado na cria��o da tabela e inser��o
	private String primeiroNome;

	@Column(table = "cliente_detalhe", length = 30, nullable = false)
	@Enumerated(EnumType.STRING)
	private SexoClienteEnum sexo;
	
	@Column(name = "data_nascimento", table = "cliente_detalhe")
	private LocalDate dataNascimento;

	@OneToMany(mappedBy = "cliente")
	private List<Pedido> pedidos;
	
	@ElementCollection
	@CollectionTable(name = "cliente_contato", joinColumns = @JoinColumn(name = "cliente_id", foreignKey = @ForeignKey(name = "fk_cliente_contato")))
	@MapKeyColumn(name = "tipo") // Mapeando mapas com @ElementCollection
	@Column(name = "descricao")
	private Map<String, String> contatos;

	@PostLoad
	public void configurarPrimeiroNome() {
		if (nome != null && !nome.isBlank()) {
			int index = nome.indexOf(" ");

			if (index > -1) {
				primeiroNome = nome.substring(0, index);
			}
		}
	}

}
