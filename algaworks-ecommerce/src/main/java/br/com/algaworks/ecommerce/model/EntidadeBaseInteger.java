package br.com.algaworks.ecommerce.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@MappedSuperclass
public class EntidadeBaseInteger {

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "data_criacao", updatable = false)
	private LocalDateTime dataCriacao; // datetime(6) not null

	@Column(name = "data_ultima_atualizacao", insertable = false)
	private LocalDateTime dataUltimaAtualizacao;
	
	@Version
	private Integer versao;

	@PrePersist
	public void aoPersistir() {
		System.out.println("Ao persistir");
		dataCriacao = LocalDateTime.now();
	}

	@PreUpdate
	public void aoAtualizar() {
		System.out.println("Ao atualizar");
		dataUltimaAtualizacao = LocalDateTime.now();
	}

}
