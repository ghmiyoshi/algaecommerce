package br.com.algaworks.ecommerce.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.hibernate.engine.spi.PersistentAttributeInterceptable;
import org.hibernate.engine.spi.PersistentAttributeInterceptor;

import br.com.algaworks.ecommerce.listener.GenericoListener;
import br.com.algaworks.ecommerce.listener.GerarNotaFiscalListener;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EntityListeners({ GerarNotaFiscalListener.class, GenericoListener.class })
//@Cacheable(false) false para DISABLE_SELECTIVE e true para ENABLE_SELECTIVE
@Entity
@Table(name = "pedido")
public class Pedido extends EntidadeBaseInteger implements PersistentAttributeInterceptable{

	@Column(name = "data_conclusao")
	private LocalDateTime dataConclusao;

	@Column(length = 30, nullable = false)
	@Enumerated(EnumType.STRING)
	private StatusPedidoEnum status;

	@Column(precision = 10, scale = 2, nullable = false)
	private BigDecimal total;

	@Embedded
	private EnderecoEntregaPedido enderecoEntrega;

	@ManyToOne(optional = false, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "cliente_id", 
				nullable = false,
				foreignKey = @ForeignKey(name = "fk_pedido_cliente"))
	private Cliente cliente;

	@LazyToOne(LazyToOneOption.NO_PROXY)
	@OneToOne(mappedBy = "pedido", fetch = FetchType.LAZY)
	private NotaFiscal notaFiscal;

	@OneToMany(mappedBy = "pedido") // cascade = CascadeType.PERSIST, orphanRemoval = true
	private List<ItemPedido> itens = new ArrayList<>();

	@LazyToOne(LazyToOneOption.NO_PROXY)
	@OneToOne(mappedBy = "pedido", fetch = FetchType.LAZY)
	private Pagamento pagamento;
	
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.NONE)
	@Transient
	private PersistentAttributeInterceptor persistentAttributeInterceptor;
	
	public NotaFiscal getNotaFiscal() {
		if(this.persistentAttributeInterceptor != null) {
			return (NotaFiscal) persistentAttributeInterceptor.readObject(this, "notaFiscal", this.notaFiscal);
		}
		
		return this.notaFiscal;
	}
	
	public void setNotaFiscal(NotaFiscal nf) {
		if(this.persistentAttributeInterceptor != null) {
			this.notaFiscal = (NotaFiscal) persistentAttributeInterceptor.writeObject(this, "notaFiscal", this.notaFiscal, nf);
		} else {
			this.notaFiscal = nf;
		}
	}
	
	public Pagamento getPagamento() {
		if(this.persistentAttributeInterceptor != null) {
			return (Pagamento) persistentAttributeInterceptor.readObject(this, "pagamento", this.pagamento);
		}
		
		return this.pagamento;
	}
	
	public void setPagamento(Pagamento pagamento) {
		if(this.persistentAttributeInterceptor != null) {
			this.pagamento = (Pagamento) persistentAttributeInterceptor.writeObject(this, "pagamento", this.pagamento, pagamento);
		} else {
			this.pagamento = pagamento;
		}
		
	}

	//@PrePersist // N�o posso ter dois callbacks
	//@PreUpdate
	public void calcularTotal() {
		if (!itens.isEmpty()) {
			total = itens.stream()
						 .map(i -> new BigDecimal(i.getQuantidade()).multiply(i.getPrecoProduto()))
						 .reduce(BigDecimal.ZERO, BigDecimal::add);
		} else {
			total = BigDecimal.ZERO;
		}
	}
	
	@PreUpdate
	@PrePersist
	public void aoPersistirPedido() {
		calcularTotal();
	}
	
	public boolean isPago() {
		return status.equals(StatusPedidoEnum.PAGO);
	}

	@Override
	public PersistentAttributeInterceptor $$_hibernate_getInterceptor() {
		return this.persistentAttributeInterceptor;
	}

	@Override
	public void $$_hibernate_setInterceptor(PersistentAttributeInterceptor interceptor) {
		this.persistentAttributeInterceptor = interceptor;
	}

}
