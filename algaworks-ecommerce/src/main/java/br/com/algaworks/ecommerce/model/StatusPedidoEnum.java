package br.com.algaworks.ecommerce.model;

public enum StatusPedidoEnum {

	AGUARDANDO, CANCELADO, PAGO

}
