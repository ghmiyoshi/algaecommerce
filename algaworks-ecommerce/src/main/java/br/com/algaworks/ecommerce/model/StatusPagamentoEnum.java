package br.com.algaworks.ecommerce.model;

public enum StatusPagamentoEnum {

	PROCESSANDO, CANCELADO, RECEBIDO

}