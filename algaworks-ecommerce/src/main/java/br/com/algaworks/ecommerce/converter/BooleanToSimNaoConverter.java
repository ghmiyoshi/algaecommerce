package br.com.algaworks.ecommerce.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class BooleanToSimNaoConverter implements AttributeConverter<Boolean, String> {

	@Override
	public String convertToDatabaseColumn(Boolean attribute) {
		return Boolean.TRUE.equals(attribute) ? "SIM" : "N�O";
	}

	@Override
	public Boolean convertToEntityAttribute(String dbData) {
		return "SIM".equals(dbData) ? true : false;
	}

}
