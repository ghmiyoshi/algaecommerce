package br.com.algaworks.ecommerce.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.algaworks.ecommerce.converter.BooleanToSimNaoConverter;
import br.com.algaworks.ecommerce.listener.GenericoListener;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@EntityListeners({ GenericoListener.class })
@Table(name = "produto", uniqueConstraints = { @UniqueConstraint(name = "unq_nome", columnNames = {"nome"})},
						 indexes = {@Index(name = "idx_nome", columnList = "nome") })
public class Produto extends EntidadeBaseInteger {

	@Column(length = 100, nullable = false) // nome varchar(100) not null
	private String nome;

	@Lob
	//@Column(columnDefinition = "varchar(275) not null")
	private String descricao;

	@Column(precision = 10, scale = 2) // preco decimal(19,2) ex: 123456789,23
	private BigDecimal preco;
	
	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinTable(name = "produto_categoria", 
			   joinColumns = @JoinColumn(name = "produto_id", nullable = false,
			   		foreignKey = @ForeignKey(name= "fk_produto_categoria_produto")),
			   inverseJoinColumns = @JoinColumn(name = "categoria_id", nullable = false,
			   		foreignKey = @ForeignKey(name = "fk_produto_categoria_categoria")))
	private List<Categoria> categorias;

	@OneToOne(mappedBy = "produto")
	private Estoque estoque;
	
	@ElementCollection // Mapeando cole��es de tipos b�sicos
	@CollectionTable(name = "produto_tag", joinColumns = @JoinColumn(name = "produto_id", foreignKey = @ForeignKey(name = "fk_produto_tags")))
	@Column(name = "tag", length = 50, nullable = false)
	private List<String> tags;
	
	@ElementCollection // Mapeando cole��es de objetos embutidos
	@JoinTable(name = "produto_atributo", joinColumns = @JoinColumn(name = "produto_id", foreignKey = @ForeignKey(name = "fk_produto_atributos")))
	@Column(name = "atributos")
	private List<Atributo> atributos;

	@Lob
	private byte[] foto;
	
	@Convert(converter =  BooleanToSimNaoConverter.class)
	@Column(length = 3, nullable = false)
	private Boolean ativo = Boolean.FALSE;
	
}
