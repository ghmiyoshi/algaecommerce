package br.com.algaworks.ecommerce.listener;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import br.com.algaworks.ecommerce.model.Pedido;
import br.com.algaworks.ecommerce.service.NotaFiscalService;

public class GerarNotaFiscalListener {
	
	private NotaFiscalService nf = new NotaFiscalService();
	
	@PrePersist
	@PreUpdate
	public void gerar(Pedido pedido) {
		if(pedido.isPago() && pedido.getNotaFiscal() == null) {
			nf.gerar(pedido);
		}
	}

}
