package br.com.algaworks.ecommerce.listener;

import javax.persistence.PostLoad;

public class GenericoListener {

	@PostLoad
	public void logCarregamento(Object object) {
		System.out.printf("Entidade %s foi carregada", object.getClass().getSimpleName());
	}

}
