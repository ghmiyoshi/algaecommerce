package br.com.algaworks.ecommerce.conhecendoentitymanager;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Pedido;
import br.com.algaworks.ecommerce.model.StatusPedidoEnum;

public class FlushTest extends EntityManagerTest {

	@Test(expected = Exception.class)
	public void chamarFlush() {
		try {
			entityManager.getTransaction().begin();
			
			Pedido pedido = entityManager.find(Pedido.class, 1);
			pedido.setStatus(StatusPedidoEnum.CANCELADO);
			
			//entityManager.flush();

			if (!pedido.getStatus().equals(StatusPedidoEnum.PAGO)) {
				throw new RuntimeException("Pedido n�o foi pago!");
			}
			
			// uma consulta obriga o JPA a sincronizar o que ele tem na mem�ria
			Pedido pedidoPago = entityManager.createQuery("SELECT p FROM Pedido p WHERE p.id = 1", Pedido.class).getSingleResult();
			
			assertEquals(pedido.getStatus(), pedidoPago.getStatus());
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
			throw e;
		}
	}

}
