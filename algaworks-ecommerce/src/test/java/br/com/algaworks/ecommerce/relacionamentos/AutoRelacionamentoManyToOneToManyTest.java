package br.com.algaworks.ecommerce.relacionamentos;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.junit.Assert;
import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Cliente;
import br.com.algaworks.ecommerce.model.ItemPedido;
import br.com.algaworks.ecommerce.model.ItemPedidoId;
import br.com.algaworks.ecommerce.model.Pedido;
import br.com.algaworks.ecommerce.model.Produto;
import br.com.algaworks.ecommerce.model.StatusPedidoEnum;

public class AutoRelacionamentoManyToOneToManyTest extends EntityManagerTest {

	@Test
	public void verificarRelacionamento() {
		Cliente cliente = entityManager.find(Cliente.class, 1);

		Pedido pedido = new Pedido();
		pedido.setStatus(StatusPedidoEnum.AGUARDANDO);
		pedido.setDataCriacao(LocalDateTime.now());
		pedido.setCliente(cliente);
		pedido.setTotal(BigDecimal.TEN);

		entityManager.getTransaction().begin();
		entityManager.persist(pedido);
		entityManager.getTransaction().commit();

		entityManager.clear();

		Cliente clienteVerificacao = entityManager.find(Cliente.class, cliente.getId());

		Assert.assertFalse(clienteVerificacao.getPedidos().isEmpty());
	}

	@Test
	public void verificarRelacionamentoItemPedido() {
		Cliente cliente = entityManager.find(Cliente.class, 1);
		Produto produto = entityManager.find(Produto.class, 1);
		Pedido pedido = new Pedido();

		pedido.setCliente(cliente);
		pedido.setTotal(BigDecimal.TEN);
		pedido.setDataCriacao(LocalDateTime.now());
		pedido.setStatus(StatusPedidoEnum.AGUARDANDO);

		ItemPedido itemPedido = new ItemPedido();
		// itemPedido.setPedidoId(pedito.getId()); IdClass
		// itemPedido.setProdutoId(produto.getId()); IdClass
		// itemPedido.setId(new ItemPedidoId(pedito.getId(), produto.getId()));
		itemPedido.setId(new ItemPedidoId());
		itemPedido.setPedido(pedido);
		itemPedido.setProduto(produto);
		itemPedido.setPrecoProduto(BigDecimal.ONE);
		itemPedido.setQuantidade(2);

		entityManager.getTransaction().begin();
		entityManager.persist(pedido);
		entityManager.persist(itemPedido);
		entityManager.getTransaction().commit();

		entityManager.clear();

		Pedido pedidoVerificacao = entityManager.find(Pedido.class, pedido.getId());

		Assert.assertFalse(pedidoVerificacao.getItens().isEmpty());
	}

}
