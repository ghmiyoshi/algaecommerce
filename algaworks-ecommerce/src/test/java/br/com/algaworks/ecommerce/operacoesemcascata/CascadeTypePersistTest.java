package br.com.algaworks.ecommerce.operacoesemcascata;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Categoria;
import br.com.algaworks.ecommerce.model.Cliente;
import br.com.algaworks.ecommerce.model.ItemPedido;
import br.com.algaworks.ecommerce.model.ItemPedidoId;
import br.com.algaworks.ecommerce.model.Pedido;
import br.com.algaworks.ecommerce.model.Produto;
import br.com.algaworks.ecommerce.model.SexoClienteEnum;
import br.com.algaworks.ecommerce.model.StatusPedidoEnum;

public class CascadeTypePersistTest extends EntityManagerTest {
	
	@Test
	public void persistirPedidoComItens() {
		Produto produto = entityManager.find(Produto.class, 1);
		Cliente cliente = entityManager.find(Cliente.class, 1);
		
		Pedido pedido = new Pedido();
		pedido.setCliente(cliente);
		pedido.setTotal(produto.getPreco());
		pedido.setStatus(StatusPedidoEnum.AGUARDANDO);
		
		ItemPedido itemPedido = new ItemPedido();
		itemPedido.setId(new ItemPedidoId());
		itemPedido.setPedido(pedido);
		itemPedido.setProduto(produto);
		itemPedido.setQuantidade(32);
		itemPedido.setPrecoProduto(produto.getPreco());
		
		pedido.setItens(Arrays.asList(itemPedido)); // CascadeType.PERSIST
		
		entityManager.getTransaction().begin();
		entityManager.persist(pedido);
		entityManager.getTransaction().commit();

		entityManager.clear();
		
		Pedido pedidoVerificacao = entityManager.find(Pedido.class, pedido.getId());
		
		assertNotNull(pedidoVerificacao.getItens());
		assertFalse(pedido.getItens().isEmpty());
	}
	
	@Test
	public void persistirPedidoComCliente() {
		Cliente cliente = new Cliente();
		cliente.setNome("Jo�o da Silva");
		cliente.setCpf("123456");
		cliente.setDataNascimento(LocalDate.of(2020, 10, 2));
		cliente.setSexo(SexoClienteEnum.MASCULINO);
		
		Pedido pedido = new Pedido();
		pedido.setCliente(cliente);
		pedido.setTotal(BigDecimal.ONE);
		pedido.setStatus(StatusPedidoEnum.AGUARDANDO);
		
		entityManager.getTransaction().begin();
		entityManager.persist(pedido);
		entityManager.getTransaction().commit();
		
		entityManager.clear();
		
		Cliente clienteVerificacao = entityManager.find(Cliente.class, cliente.getId());
		assertNotNull(clienteVerificacao);
	}
	
	@Test
	public void persistirItemPedidoComPedido() {
		Cliente cliente = entityManager.find(Cliente.class, 1);
		Produto produto = entityManager.find(Produto.class, 1);
		
		Pedido pedido = new Pedido();
		pedido.setCliente(cliente);
		pedido.setTotal(BigDecimal.ONE);
		pedido.setStatus(StatusPedidoEnum.AGUARDANDO);
		
		ItemPedido itemPedido = new ItemPedido();
		itemPedido.setId(new ItemPedidoId());
		itemPedido.setPedido(pedido); // N�o � necess�rio CascadeType.PERSIST porque possui @MapsId, o Hibernate entende que deve inserir primeiro o pedido e depois o itemPedido
		itemPedido.setProduto(produto);
		itemPedido.setQuantidade(32);
		itemPedido.setPrecoProduto(produto.getPreco());
		
		entityManager.getTransaction().begin();
		entityManager.persist(itemPedido);
		entityManager.getTransaction().commit();
		
		Pedido pedidoVerificacao = entityManager.find(Pedido.class, pedido.getId());
		
		assertNotNull(pedidoVerificacao);
	}
	
	@Test
	public void persistirProdutoComCategoria() {
		Produto produto = new Produto();
		produto.setNome("PS5");
		produto.setDescricao("Video Game");
		produto.setPreco(BigDecimal.ONE);
		produto.setDataCriacao(LocalDateTime.now());
		produto.setDataUltimaAtualizacao(LocalDateTime.now());
		
		Categoria categoria = new Categoria();
		Categoria categoria2 = new Categoria();
		categoria.setNome("Eletr�nicos");
		categoria2.setNome("Jogos");

		produto.setCategorias(Arrays.asList(categoria, categoria2));
		
		entityManager.getTransaction().begin();
		entityManager.persist(produto);
		entityManager.getTransaction().commit();

		entityManager.clear();
		
		Produto produtoVerificacao = entityManager.find(Produto.class, produto.getId());
		
		assertNotNull(produtoVerificacao);
		assertFalse(produtoVerificacao.getCategorias().isEmpty());
	}

}
