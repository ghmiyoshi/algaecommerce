package br.com.algaworks.ecommerce.relacionamentos;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.junit.Assert;
import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Cliente;
import br.com.algaworks.ecommerce.model.Pedido;
import br.com.algaworks.ecommerce.model.Produto;
import br.com.algaworks.ecommerce.model.StatusPedidoEnum;

public class CallbacksTest extends EntityManagerTest {
	
	@Test
	public void loadListener() {
		Produto produto = new Produto();
		produto.setNome("Nintendo Switch");
		produto.setDescricao("Console");
		
		entityManager.getTransaction().begin();
		entityManager.persist(produto);
		entityManager.getTransaction().commit();
		entityManager.clear();
		
		Produto produtoVerificacao = entityManager.find(Produto.class, produto.getId());
		assertEquals("Nintendo Switch", produtoVerificacao.getNome());
	}

	@Test
	public void callback() {
		Cliente cliente = entityManager.find(Cliente.class, 1);
		Pedido pedido = new Pedido();

		pedido.setDataCriacao(LocalDateTime.now());
		pedido.setTotal(new BigDecimal(119.98));
		pedido.setCliente(cliente);
		pedido.setStatus(StatusPedidoEnum.AGUARDANDO);

		entityManager.getTransaction().begin();
		entityManager.persist(pedido);

		pedido.setStatus(StatusPedidoEnum.PAGO);

		entityManager.getTransaction().commit();
		entityManager.clear();

		Pedido pedidoVerificacao = entityManager.find(Pedido.class, pedido.getId());

		Assert.assertNotNull(pedidoVerificacao.getDataCriacao());
		Assert.assertNotNull(pedidoVerificacao.getDataUltimaAtualizacao());
	}

}
