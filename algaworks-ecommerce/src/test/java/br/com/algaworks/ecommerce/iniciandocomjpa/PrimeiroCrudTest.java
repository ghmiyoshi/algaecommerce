package br.com.algaworks.ecommerce.iniciandocomjpa;

import java.util.List;

import javax.persistence.TypedQuery;

import org.junit.Assert;
import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Cliente;
import br.com.algaworks.ecommerce.model.SexoClienteEnum;

public class PrimeiroCrudTest extends EntityManagerTest {

	@Test
	public void inserirClienteComPersist() {
		Cliente cliente = new Cliente();
		cliente.setNome("Gabriel");
		cliente.setCpf("12345678918");
		cliente.setSexo(SexoClienteEnum.MASCULINO);

		entityManager.getTransaction().begin();
		entityManager.persist(cliente);
		entityManager.getTransaction().commit();

		entityManager.clear();
		Cliente clienteVerificacao = entityManager.find(Cliente.class, cliente.getId());

		Assert.assertNotNull(clienteVerificacao);
		Assert.assertEquals("Gabriel", clienteVerificacao.getNome());
	}
	
	@Test
	public void inserirClienteComMerge() {
		Cliente cliente = new Cliente();
		cliente.setNome("Gabriel");
		cliente.setCpf("12345678911");
		cliente.setSexo(SexoClienteEnum.MASCULINO);

		entityManager.getTransaction().begin();
		cliente = entityManager.merge(cliente);
		entityManager.getTransaction().commit();

		entityManager.clear();
		Cliente clienteVerificacao = entityManager.find(Cliente.class, cliente.getId());

		Assert.assertNotNull(clienteVerificacao);
		Assert.assertEquals("Gabriel", clienteVerificacao.getNome());
	}
	
	@Test
	public void atualizarCliente() {
		Cliente cliente = new Cliente();
		cliente.setNome("Gabriel");
		cliente.setSexo(SexoClienteEnum.MASCULINO);
		cliente.setCpf("12345678912");

		entityManager.getTransaction().begin();
		cliente = entityManager.merge(cliente);
		cliente.setNome("Hideki");
		entityManager.getTransaction().commit();

		entityManager.clear();
		Cliente clienteVerificacao = entityManager.find(Cliente.class, cliente.getId());

		Assert.assertNotNull(clienteVerificacao);
		Assert.assertEquals("Hideki", clienteVerificacao.getNome());
	}
	
	@Test
	public void listarCliente() {
		TypedQuery<Cliente> typedQuery = entityManager.createQuery("SELECT c FROM Cliente c", Cliente.class);
		List<Cliente> clientes = typedQuery.getResultList();
		
		Assert.assertEquals(5, clientes.size());
	}
	
	@Test
	public void pesquisarCliente() {
		Cliente cliente = entityManager.find(Cliente.class, 1);
		
		Assert.assertEquals("Fernando Medeiros", cliente.getNome());
	}
	
	@Test
	public void removerCliente() {
		Cliente cliente = entityManager.find(Cliente.class, 3);
		
		entityManager.remove(cliente);
		
		entityManager.getTransaction().begin();
		entityManager.getTransaction().commit();
		
		Cliente clienteVerificacao = entityManager.find(Cliente.class, 3);
		
		Assert.assertNull(clienteVerificacao);
	}
	
	

}
