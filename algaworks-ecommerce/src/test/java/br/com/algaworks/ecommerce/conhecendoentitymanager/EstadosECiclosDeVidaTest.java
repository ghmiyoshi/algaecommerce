package br.com.algaworks.ecommerce.conhecendoentitymanager;

import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Categoria;

public class EstadosECiclosDeVidaTest extends EntityManagerTest {
	
	@Test
	public void analisarEstados() {
		Categoria categoriaNova = new Categoria(); // estado transient	
		Categoria categoriaGerenciadaRetorno = entityManager.merge(categoriaNova); // estado managed
		
		Categoria categoriaGerenciada = entityManager.find(Categoria.class, 1L); // estado managed
		entityManager.remove(categoriaGerenciada); // estado removed
		entityManager.persist(categoriaGerenciada); // estado managed
		entityManager.detach(categoriaGerenciada); // estado detached
	}

}
