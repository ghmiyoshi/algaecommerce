package br.com.algaworks.ecommerce.certification;

public interface Black {
	default void getColor() {
		System.out.print("Black");
	}
}

interface Red extends Black {
	default void getColor() {
		System.out.print("Red");
	}
}

interface Gold {
	static void getColor() {
		System.out.print("Gold");
	}
}

class Colors implements Black, Red, Gold {
	public static void main(String[] args) {
		Colors colors = new Colors();
		colors.getColor();
	}
}
