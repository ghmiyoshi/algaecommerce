package br.com.algaworks.ecommerce.problemalazyhibernate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Pedido;

public class OneLazyTest extends EntityManagerTest {

	@Test
	public void mostrarProblema() {
		System.out.println("Buscando um pedido...");
		Pedido pedido = entityManager.find(Pedido.class, 1);
		assertNotNull(pedido);
		
		System.out.println("----------------------------------------------------");
		
		System.out.println("Buscando uma lista de pedidos...");
		
		List<Pedido> resultList = entityManager.createQuery("SELECT p FROM Pedido p", Pedido.class).getResultList();
		assertFalse(resultList.isEmpty());
	}

}
