package br.com.algaworks.ecommerce.mapeamentoavancado;

import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Cliente;
import br.com.algaworks.ecommerce.model.SexoClienteEnum;

public class SecondaryTableTest extends EntityManagerTest {

	@Test
	public void salvarCliente() {
		Cliente cliente = new Cliente();

		cliente.setNome("Carlos");
		cliente.setCpf("12345678912");
		cliente.setSexo(SexoClienteEnum.MASCULINO);
		cliente.setDataNascimento(LocalDate.now());

		entityManager.getTransaction().begin();
		entityManager.persist(cliente);
		entityManager.getTransaction().commit();

		entityManager.clear();

		Cliente clienteVerificacao = entityManager.find(Cliente.class, cliente.getId());

		Assert.assertNotNull(clienteVerificacao.getSexo());
	}
}
