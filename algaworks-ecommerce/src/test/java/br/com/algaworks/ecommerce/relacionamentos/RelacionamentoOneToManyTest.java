package br.com.algaworks.ecommerce.relacionamentos;

import org.junit.Assert;
import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Categoria;

public class RelacionamentoOneToManyTest extends EntityManagerTest {

	@Test
	public void verificarAutoRelacionamento() {
		Categoria categoriaPai = new Categoria();
		categoriaPai.setNome("Eletrônicos");

		Categoria categoriaFilha = new Categoria();
		categoriaFilha.setNome("Celulares");
		categoriaFilha.setCategoriaPai(categoriaPai);

		entityManager.getTransaction().begin();
		entityManager.persist(categoriaPai);
		entityManager.persist(categoriaFilha);
		entityManager.getTransaction().commit();

		entityManager.clear();

		Categoria categoriaFilhaVerificacao = entityManager.find(Categoria.class, categoriaFilha.getId());
		Categoria categoriaPaiVerificacao = entityManager.find(Categoria.class, categoriaPai.getId());

		Assert.assertNotNull(categoriaFilhaVerificacao.getCategoriaPai());
		Assert.assertFalse(categoriaPaiVerificacao.getCategorias().isEmpty());
	}

}
