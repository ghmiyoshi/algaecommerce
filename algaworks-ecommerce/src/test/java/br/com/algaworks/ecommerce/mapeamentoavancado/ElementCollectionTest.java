package br.com.algaworks.ecommerce.mapeamentoavancado;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Assert;
import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Atributo;
import br.com.algaworks.ecommerce.model.Cliente;
import br.com.algaworks.ecommerce.model.Produto;

public class ElementCollectionTest extends EntityManagerTest {

	@Test
	public void aplicarTags() {
		entityManager.getTransaction().begin();
		Produto produto = new Produto();
		
		produto.setNome("PS5");
		produto.setDescricao("Video Game");
		produto.setTags(Arrays.asList("ebook", "livro-digital"));
		entityManager.persist(produto);
		entityManager.getTransaction().commit();

		entityManager.clear();

		Produto produtoVerificacao = entityManager.find(Produto.class, produto.getId());

		Assert.assertNotNull(produtoVerificacao.getTags().isEmpty());
	}

	@Test
	public void aplicarAtributos() {
		entityManager.getTransaction().begin();
		Produto produto = entityManager.find(Produto.class, 1);
		produto.setAtributos(Arrays.asList(new Atributo("tela", "1280 X 720")));
		entityManager.getTransaction().commit();

		entityManager.clear();

		Produto produtoVerificacao = entityManager.find(Produto.class, produto.getId());

		Assert.assertNotNull(produtoVerificacao.getAtributos().isEmpty());
	}

	@Test
	public void aplicarContatos() {
		entityManager.getTransaction().begin();
		Cliente cliente = entityManager.find(Cliente.class, 1);

		cliente.setContatos(Collections.singletonMap("email", "gabriel@gmail.com"));

		entityManager.getTransaction().commit();

		entityManager.clear();

		Cliente clienteVerificacao = entityManager.find(Cliente.class, cliente.getId());

		Assert.assertNotNull(clienteVerificacao.getContatos());
		Assert.assertEquals("gabriel@gmail.com", clienteVerificacao.getContatos().get("email"));
	}

}
