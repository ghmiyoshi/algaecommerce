package br.com.algaworks.ecommerce.jpql;

import static org.junit.Assert.assertFalse;

import java.util.List;

import javax.persistence.TypedQuery;

import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;

public class FuncoesNumeroTest extends EntityManagerTest {
	
	@Test
	public void aplicarFuncaoNumero() {
		// Abs - valor absoluto | Mod - Resto da divis�o | Sqrt - Raiz quadrada 
		String jpql = "SELECT abs(-10), mod(3,2), sqrt(9) from Pedido";
		
		TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
		
		resultList.forEach(arr -> System.out.println("Abs: " + arr[0] + " | Mod: " + arr[1] + " | Sqrt: " + arr[2]));
	}

}
