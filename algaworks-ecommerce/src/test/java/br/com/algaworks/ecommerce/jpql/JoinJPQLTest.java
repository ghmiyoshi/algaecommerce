package br.com.algaworks.ecommerce.jpql;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.persistence.TypedQuery;

import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Pedido;

public class JoinJPQLTest extends EntityManagerTest {
	
	@Test
	public void fazerJoin() {
		/* Busco os pedidos que tem correspond�ncia com pagamento, ou seja, preciso ter um pedido 
		   que obrigatoriamente tenha um pagamento e que o pagamento tenha o pedido */
		String jpql = "SELECT p FROM Pedido p JOIN p.pagamento pag";
		
		TypedQuery<Pedido> typedQuery = entityManager.createQuery(jpql, Pedido.class);
		List<Pedido> resultList = typedQuery.getResultList();
		
		assertTrue(resultList.size() == 1);
	}
	
	@Test
	public void fazerJoin2() {
		/* Busco os pedidos e itens que tem correspond�ncia com item_pedido. Nesse caso, como � uma lista de
		   itens, o registro de pedido ir� duplicar */
		String jpql = "SELECT p, i FROM Pedido p JOIN p.itens i";
		
		TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		assertTrue(resultList.size() == 3);
	}
	
	@Test
	public void fazerJoin3() {
		/* Busco os pedidos e produto que tem correspond�ncia com item_pedido e produto e id do produto igual a 1.
		   Nesse caso, como o pedido possui dois itens, o registro de pedido ir� duplicar */
		String jpql = "SELECT p, prod FROM Pedido p JOIN p.itens i JOIN i.produto prod WHERE prod.id = 1";
		
		TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		assertTrue(resultList.size() == 2);
	}
	
	@Test
	public void fazerLeftJoin() {
		// Busco os pedidos que tem e n�o tem correspond�ncia com pagamento 
		String jpql = "SELECT p FROM Pedido p LEFT JOIN p.pagamento pag ON pag.status = 'PROCESSANDO'";
		
		TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
	}
	
	@Test
	public void joinFetch() {
		/* Uso o FETCH para buscar tudo em uma query s�, solu��o do problema N + 1.
		   Busco todos os pedidos que tem ou n�o correspond�ncia com pagamento, todos que tem correspond�ncia
		   com cliente e todos que tem ou n�o correspond�ncia com nota fiscal. */
		String jpql = "SELECT p FROM Pedido p "
					+ "LEFT JOIN FETCH p.pagamento "
					+ "JOIN FETCH p.cliente "
					+ "LEFT JOIN FETCH p.notaFiscal ";
		
		TypedQuery<Pedido> typedQuery = entityManager.createQuery(jpql, Pedido.class);
		List<Pedido> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
	}
	
	@Test
	public void pathExpressions() {
		String jpql = "SELECT p.cliente.nome FROM Pedido p";
		
		TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
		assertEquals(resultList.get(0), "Fernando Medeiros");
	}
	
	@Test
	public void buscarPedidosComProdutoEspecifico() {
		String jpql = "SELECT p FROM Pedido p JOIN p.itens i JOIN i.produto prod WHERE prod.id = 1";
		// String jpql = "SELECT p FROM Pedido p JOIN p.itens i WHERE i.produto.id = 1";
		// String jpql = "SELECT p FROM Pedido p JOIN p.itens i WHERE i.id.produtoId = 1";
		
		TypedQuery<Pedido> typedQuery = entityManager.createQuery(jpql, Pedido.class);
		List<Pedido> resultList = typedQuery.getResultList();
		
		assertTrue(resultList.size() == 2);
		assertFalse(resultList.isEmpty());
	}

}
