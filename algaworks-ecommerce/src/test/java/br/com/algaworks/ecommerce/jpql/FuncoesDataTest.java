package br.com.algaworks.ecommerce.jpql;

import static org.junit.Assert.assertFalse;

import java.util.List;
import java.util.TimeZone;

import javax.persistence.TypedQuery;

import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;

public class FuncoesDataTest extends EntityManagerTest {
	
	@Test
	public void aplicarFuncaoData() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		
		String jpql = "SELECT current_date, current_time, current_timestamp FROM Pedido p";
		
		TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
		
		resultList.forEach(data -> System.out.println("Date: " + data[0] + " | Time: " + data[1] + " | Timestamp: " + data[2]));
	}
	
	@Test
	public void aplicarFuncaoDataYearMonthDay() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		
		String jpql = "SELECT year(p.dataCriacao), month(p.dataCriacao), day(p.dataCriacao) FROM Pedido p";
		
		TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
		
		resultList.forEach(data -> System.out.println("Ano: " + data[0] + " | M�s: " + data[1] + " | Dia: " + data[2]));
	}
	
	@Test
	public void aplicarFuncaoHourMinuteSecond() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		
		String jpql = "SELECT hour(p.dataCriacao), minute(p.dataCriacao), second(p.dataCriacao) FROM Pedido p";
		
		TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
		
		resultList.forEach(data -> System.out.println("Horas: " + data[0] + " | Minutos: " + data[1] + " | Segundos: " + data[2]));
	}

}
