package br.com.algaworks.ecommerce.jpql;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.TypedQuery;

import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Cliente;
import br.com.algaworks.ecommerce.model.Pedido;
import br.com.algaworks.ecommerce.model.Produto;

public class ExpressoesCondicionaisTest extends EntityManagerTest {
	
	@Test
	public void usarExpressaoCondicionalLike() {
		String jpql = "SELECT c FROM Cliente c WHERE c.nome LIKE concat(:nome, '%')";
		
		TypedQuery<Cliente> typedQuery = entityManager.createQuery(jpql, Cliente.class);
		typedQuery.setParameter("nome", "Fernando");
		
		List<Cliente> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
	}
	
	@Test
	public void usarExpressaoCondicionalLike2() {
		String jpql = "SELECT c FROM Cliente c WHERE c.nome LIKE concat('%', :nome, '%')";
		
		TypedQuery<Cliente> typedQuery = entityManager.createQuery(jpql, Cliente.class);
		typedQuery.setParameter("nome", "a");
		
		List<Cliente> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
	}
	
	@Test
	public void usarExpressaoCondicionalNull() {
		String jpql = "SELECT p FROM Produto p WHERE p.descricao is null";
		//String jpql = "SELECT p FROM Produto p WHERE p.descricao is not null";
		
		TypedQuery<Produto> typedQuery = entityManager.createQuery(jpql, Produto.class);
		
		List<Produto> resultList = typedQuery.getResultList();
		
		assertTrue(resultList.isEmpty());
	}
	
	@Test
	public void usarExpressaoCondicionalEmpty() {
		// Empty � usado para cole��es
		String jpql = "SELECT p FROM Produto p WHERE p.categorias is empty";
		//String jpql = "SELECT p FROM Produto p WHERE p.categorias is not empty";
		
		TypedQuery<Produto> typedQuery = entityManager.createQuery(jpql, Produto.class);
		
		List<Produto> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
	}
	
	@Test
	public void usarMaiorMenor() {
		String jpql = "SELECT p FROM Produto p WHERE p.preco >= :preco";
		
		TypedQuery<Produto> typedQuery = entityManager.createQuery(jpql, Produto.class);
		typedQuery.setParameter("preco", new BigDecimal(499));
		
		List<Produto> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
	}
	
	@Test
	public void usarMaiorMenor2() {
		String jpql = "SELECT p FROM Produto p WHERE p.preco >= :precoInicial AND p.preco <= :precoFinal";
		
		TypedQuery<Produto> typedQuery = entityManager.createQuery(jpql, Produto.class);
		typedQuery.setParameter("precoInicial", new BigDecimal(400));
		typedQuery.setParameter("precoFinal", new BigDecimal(1500));
		
		List<Produto> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
	}
	
	@Test
	public void usarMaiorMenorData() {
		String jpql = "SELECT p FROM Pedido p WHERE p.dataCriacao <= :dataInicial";
		
		TypedQuery<Pedido> typedQuery = entityManager.createQuery(jpql, Pedido.class);
		typedQuery.setParameter("dataInicial", LocalDateTime.now().minusDays(2));
		
		List<Pedido> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
	}
	
	@Test
	public void usarBetween() {
		String jpql = "SELECT p FROM Produto p WHERE p.preco BETWEEN :precoInicial AND :precoFinal";
		
		TypedQuery<Produto> typedQuery = entityManager.createQuery(jpql, Produto.class);
		typedQuery.setParameter("precoInicial", new BigDecimal(400));
		typedQuery.setParameter("precoFinal", new BigDecimal(1500));
		
		List<Produto> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
	}
	
	@Test
	public void usarBetweenData() {
		String jpql = "SELECT p FROM Pedido p WHERE p.dataCriacao BETWEEN :dataInicial AND :dataFinal";
		
		TypedQuery<Pedido> typedQuery = entityManager.createQuery(jpql, Pedido.class);
		typedQuery.setParameter("dataInicial", LocalDateTime.now().minusDays(2));
		typedQuery.setParameter("dataFinal", LocalDateTime.now());
		
		List<Pedido> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
	}
	
	@Test
	public void usarOperadorLogicoAnd() {
		String jpql = "SELECT p FROM Pedido p WHERE p.total > 500 AND p.status = 'AGUARDANDO' AND p.cliente.id = 1";
		
		TypedQuery<Pedido> typedQuery = entityManager.createQuery(jpql, Pedido.class);
		
		List<Pedido> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
	}
	
	@Test
	public void usarOperadorLogicoOr() {
		String jpql = "SELECT p FROM Pedido p WHERE p.total > 500 AND p.status = 'AGUARDANDO' OR p.status = 'PAGO'";
		
		TypedQuery<Pedido> typedQuery = entityManager.createQuery(jpql, Pedido.class);
		
		List<Pedido> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
	}

}
