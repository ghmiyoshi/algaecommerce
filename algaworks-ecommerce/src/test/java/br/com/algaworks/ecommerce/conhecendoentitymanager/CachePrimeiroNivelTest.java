package br.com.algaworks.ecommerce.conhecendoentitymanager;

import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Produto;

public class CachePrimeiroNivelTest extends EntityManagerTest {
	
	@Test
	public void verificarCache() {
		Produto produto = entityManager.find(Produto.class, 1L);
		System.out.println(produto.getNome());
		System.out.println("------------------------------------");
		// entityManager.clear(); limpa a mem�ria do entity manager
		Produto produtoResgatado = entityManager.find(Produto.class, produto.getId()); // busca da mem�ria, n�o executa outro SELECT
		System.out.println(produtoResgatado.getNome());
	}

}
