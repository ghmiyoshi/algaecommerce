package br.com.algaworks.ecommerce.mapeamentoavancado;

import org.junit.Assert;
import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Cliente;

public class PropriedadesTransientesTest extends EntityManagerTest {

	@Test
	public void validarPrimeiroNome() {
		Cliente cliente = entityManager.find(Cliente.class, 1);

		Assert.assertEquals("Fernando", cliente.getPrimeiroNome());
	}

}
