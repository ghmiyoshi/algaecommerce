package br.com.algaworks.ecommerce.mapeamentoavancado;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.NotaFiscal;
import br.com.algaworks.ecommerce.model.Pedido;
import br.com.algaworks.ecommerce.model.Produto;

public class SalvandoArquivosTest extends EntityManagerTest {

	@Test
	public void salvarXmlNota() {
		Pedido pedido = entityManager.find(Pedido.class, 1);

		NotaFiscal notaFiscal = new NotaFiscal();
		notaFiscal.setPedido(pedido);
		notaFiscal.setDataEmissao(new Date());
		notaFiscal.setXml(carregarNotaFiscal("nota-fiscal.xml"));

		entityManager.getTransaction().begin();
		entityManager.persist(notaFiscal);
		entityManager.getTransaction().commit();
		
		entityManager.clear();

		NotaFiscal notaFiscalVerificacao = entityManager.find(NotaFiscal.class, notaFiscal.getId());
		Assert.assertNotNull(notaFiscalVerificacao.getXml());
		Assert.assertTrue(notaFiscalVerificacao.getXml().length > 0);

		// Salva nota fiscal na pasta do usu�rio
		try {
			OutputStream out = new FileOutputStream(
					Files.createFile(Paths.get(System.getProperty("user.home") + "/nota-fiscal.xml")).toFile());
			out.write(notaFiscalVerificacao.getXml());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	private static byte[] carregarNotaFiscal(String nome) {
		try {
			return SalvandoArquivosTest.class.getResourceAsStream("/" + nome).readAllBytes();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Test
	public void salvarFoto() {
		Produto produto = entityManager.find(Produto.class, 1);

		produto.setFoto(carregarNotaFiscal("spring.png"));

		entityManager.getTransaction().begin();
		entityManager.getTransaction().commit();
		
		entityManager.clear();

		Produto produtoVerificacao = entityManager.find(Produto.class, produto.getId());
		Assert.assertNotNull(produtoVerificacao.getFoto());
		Assert.assertTrue(produtoVerificacao.getFoto().length > 0);

		// Salva foto na pasta do usu�rio
		try {
			OutputStream out = new FileOutputStream(
					Files.createFile(Paths.get(System.getProperty("user.home") + "/spring.png")).toFile());
			out.write(produtoVerificacao.getFoto());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
