package br.com.algaworks.ecommerce.probleman1;

import static org.junit.Assert.assertFalse;

import java.util.List;

import javax.persistence.EntityGraph;

import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Pedido;

public class ProblemaN1Test extends EntityManagerTest {

	@Test
	public void resolverComEntityGraph() {
		EntityGraph<Pedido> createEntityGraph = entityManager.createEntityGraph(Pedido.class);
		createEntityGraph.addAttributeNodes("cliente", "notaFiscal", "pagamento");
		
		List<Pedido> resultList = entityManager
			.createQuery("SELECT p FROM Pedido p", Pedido.class)
			.setHint("javax.persistence.loadgraph", createEntityGraph)
			.getResultList();
		
		assertFalse(resultList.isEmpty());
	}
	
	@Test
	public void resolverComFetch() {
		String jpql = "SELECT p FROM Pedido p JOIN FETCH p.cliente cli JOIN FETCH p.pagamento pag JOIN FETCH p.notaFiscal nf";
		
		List<Pedido> resultList = entityManager
			.createQuery(jpql, Pedido.class)
			.getResultList();
		
		assertFalse(resultList.isEmpty());
	}

}
