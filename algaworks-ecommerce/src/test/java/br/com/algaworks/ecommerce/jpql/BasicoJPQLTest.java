package br.com.algaworks.ecommerce.jpql;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.persistence.TypedQuery;

import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.dto.ProdutoDTO;
import br.com.algaworks.ecommerce.model.Cliente;
import br.com.algaworks.ecommerce.model.Pedido;

public class BasicoJPQLTest extends EntityManagerTest {
	
	@Test
	public void buscaPorIdentificador() {
		TypedQuery<Pedido> typedQuery = entityManager.createQuery("SELECT p FROM Pedido p WHERE p.id = 1", Pedido.class);
		Pedido pedido = typedQuery.getSingleResult();
		
		assertNotNull(pedido);
	}
	
	@Test
	public void selecionarUmAtributoParaRetorno() {
		TypedQuery<String> typedQuery = entityManager.createQuery("SELECT p.nome FROM Produto p", String.class);
		
		List<String> nomes = typedQuery.getResultList();
		
		assertTrue(String.class.equals(nomes.get(0).getClass()));
		assertNotNull(nomes);
		
		TypedQuery<Cliente> typedQuery2 = entityManager.createQuery("SELECT p.cliente from Pedido p", Cliente.class);
		List<Cliente> clientes = typedQuery2.getResultList();
		
		assertTrue(Cliente.class.equals(clientes.get(0).getClass()));
		assertNotNull(clientes);
	}
	
	@Test
	public void projetarResultado() {
		// Trabalhando com proje��es, recupero somente o id e o nome dos produtos
		TypedQuery<Object[]> typedQuery = entityManager.createQuery("SELECT p.id, p.nome FROM Produto p", Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		assertTrue(resultList.get(0).length == 2);
		
		resultList.forEach(array -> System.out.println(array[0] + ", " + array[1]));
	}
	
	@Test
	public void projetarNoDTO() {
		String jpql = "SELECT new br.com.algaworks.ecommerce.dto.ProdutoDTO(id, nome) from Produto";
		
		TypedQuery<ProdutoDTO> typedQuery = entityManager.createQuery(jpql, ProdutoDTO.class);
		List<ProdutoDTO> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
		
		resultList.forEach(prod -> System.out.println(prod.getId() + ", " + prod.getNome()));
	}
	
	@Test
	public void ordenarResultados() {
		String jpql = "SELECT c FROM Cliente c ORDER BY c.nome DESC";
		
		TypedQuery<Cliente> typedQuery = entityManager.createQuery(jpql, Cliente.class);
		List<Cliente> resultList = typedQuery.getResultList();
		
		resultList.forEach(cliente -> System.out.println(cliente.getId() + " - " + cliente.getNome()));
		
		assertFalse(resultList.isEmpty());
	}

}
