package br.com.algaworks.ecommerce;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

public class EntityManagerTest {

	protected static EntityManagerFactory entityManagerFactory;
	protected EntityManager entityManager;

	@BeforeClass // Executado antes das classes de teste, da inicialização da execução dos testes
	public static void setUpBeforeClass() {
		entityManagerFactory = Persistence.createEntityManagerFactory("Ecommerce-PU");
	}

	@AfterClass // Executado no final da execução da classe
	public static void tearDownAfterClass() {
		entityManagerFactory.close();
	}

	@Before // Executado antes de cada um dos testes
	public void setUpBefore() {
		entityManager = entityManagerFactory.createEntityManager();
	}

	@After // Executado depois de cada um dos testes
	public void tearDownAfter() {
		entityManager.close();
	}

}
