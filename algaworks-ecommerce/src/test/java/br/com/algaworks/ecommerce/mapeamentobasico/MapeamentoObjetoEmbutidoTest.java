package br.com.algaworks.ecommerce.mapeamentobasico;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Cliente;
import br.com.algaworks.ecommerce.model.EnderecoEntregaPedido;
import br.com.algaworks.ecommerce.model.Pedido;
import br.com.algaworks.ecommerce.model.StatusPedidoEnum;

public class MapeamentoObjetoEmbutidoTest extends EntityManagerTest {

	@Test
	public void analisarMapeamentoObjetoEmbutido() {
		Cliente cliente = entityManager.find(Cliente.class, 1);
		EnderecoEntregaPedido endereco = new EnderecoEntregaPedido();
		endereco.setCep("00000-000");
		endereco.setCidade("Suzano");
		endereco.setBairro("Miguel Badra");
		endereco.setNumero("738");

		Pedido pedido = new Pedido();
		pedido.setDataCriacao(LocalDateTime.now());
		pedido.setEnderecoEntrega(endereco);
		pedido.setStatus(StatusPedidoEnum.AGUARDANDO);
		pedido.setTotal(BigDecimal.TEN);
		pedido.setCliente(cliente);

		entityManager.getTransaction().begin();
		entityManager.persist(pedido);
		entityManager.getTransaction().commit();

		entityManager.clear();

		Pedido pedidoVerificacao = entityManager.find(Pedido.class, pedido.getId());

		assertNotNull(pedidoVerificacao);
		assertNotNull(pedidoVerificacao.getEnderecoEntrega());
		assertNotNull(pedidoVerificacao.getEnderecoEntrega().getCidade());
	}

}
