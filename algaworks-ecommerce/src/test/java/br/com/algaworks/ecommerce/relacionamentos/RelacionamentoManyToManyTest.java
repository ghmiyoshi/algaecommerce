package br.com.algaworks.ecommerce.relacionamentos;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Categoria;
import br.com.algaworks.ecommerce.model.Produto;

public class RelacionamentoManyToManyTest extends EntityManagerTest {

	@Test
	public void verificarRelacionamento() {
		Produto produto = entityManager.find(Produto.class, 1);
		Categoria categoria = entityManager.find(Categoria.class, 1);

		entityManager.getTransaction().begin();
//		categoria.setProdutos(Arrays.asList(produto)); // Não salva o produto porque não é o owner da relação
		produto.setCategorias(Arrays.asList(categoria)); // Usado recurso de atualiza��o de objeto de forma gerenciado
		entityManager.getTransaction().commit();

		entityManager.clear();

		Categoria categoriaVerificacao = entityManager.find(Categoria.class, categoria.getId());

		Assert.assertFalse(categoriaVerificacao.getProdutos().isEmpty());
	}

}
