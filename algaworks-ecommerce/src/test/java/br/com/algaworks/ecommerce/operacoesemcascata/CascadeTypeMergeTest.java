package br.com.algaworks.ecommerce.operacoesemcascata;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Cliente;
import br.com.algaworks.ecommerce.model.ItemPedido;
import br.com.algaworks.ecommerce.model.ItemPedidoId;
import br.com.algaworks.ecommerce.model.Pedido;
import br.com.algaworks.ecommerce.model.Produto;
import br.com.algaworks.ecommerce.model.StatusPedidoEnum;

public class CascadeTypeMergeTest extends EntityManagerTest {

	@Test
	public void atualizarPedidoComItens() {
		Cliente cliente = entityManager.find(Cliente.class, 1);
		Produto produto = entityManager.find(Produto.class, 1);

		Pedido pedido = new Pedido();
		pedido.setId(1);
		pedido.setCliente(cliente);
		pedido.setStatus(StatusPedidoEnum.AGUARDANDO);

		ItemPedido itemPedido = new ItemPedido();
		itemPedido.setId(new ItemPedidoId());
		itemPedido.getId().setPedidoId(pedido.getId());
		itemPedido.getId().setProdutoId(produto.getId());
		itemPedido.setPedido(pedido);
		itemPedido.setProduto(produto);
		itemPedido.setQuantidade(3);
		itemPedido.setPrecoProduto(produto.getPreco());

		pedido.setItens(Arrays.asList(itemPedido)); // CascadeType.MERGE

		entityManager.getTransaction().begin();
		entityManager.merge(pedido);
		entityManager.getTransaction().commit();

		entityManager.clear();

		ItemPedido itemPedidoVerificacao = entityManager.find(ItemPedido.class, itemPedido.getId());
		Assert.assertTrue(itemPedidoVerificacao.getQuantidade().equals(3));
	}

	@Test
	public void atualizarItemPedidoComPedido() {
		Cliente cliente = entityManager.find(Cliente.class, 1);
		Produto produto = entityManager.find(Produto.class, 1);

		Pedido pedido = new Pedido();
		pedido.setId(1);
		pedido.setCliente(cliente);
		pedido.setStatus(StatusPedidoEnum.PAGO);

		ItemPedido itemPedido = new ItemPedido();
		itemPedido.setId(new ItemPedidoId());
		itemPedido.getId().setPedidoId(pedido.getId());
		itemPedido.getId().setProdutoId(produto.getId());
		itemPedido.setPedido(pedido); // CascadeType.MERGE
		itemPedido.setProduto(produto);
		itemPedido.setQuantidade(5);
		itemPedido.setPrecoProduto(produto.getPreco());

		pedido.setItens(Arrays.asList(itemPedido));

		entityManager.getTransaction().begin();
		entityManager.merge(itemPedido);
		entityManager.getTransaction().commit();

		entityManager.clear();

		ItemPedido itemPedidoVerificacao = entityManager.find(ItemPedido.class, itemPedido.getId());
		Assert.assertTrue(StatusPedidoEnum.PAGO.equals(itemPedidoVerificacao.getPedido().getStatus()));
	}

}