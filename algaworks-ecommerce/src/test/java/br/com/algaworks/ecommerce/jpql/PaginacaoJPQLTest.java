package br.com.algaworks.ecommerce.jpql;

import static org.junit.Assert.assertFalse;

import java.util.List;

import javax.persistence.TypedQuery;

import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Categoria;

public class PaginacaoJPQLTest extends EntityManagerTest {

	@Test
	public void paginarResultados() {
		String jpql = "SELECT c FROM Categoria c";

		TypedQuery<Categoria> typedQuery = entityManager.createQuery(jpql, Categoria.class);
		
		// FIRST_RESULT = MAX_RESULTS * (pagina - 1)
		typedQuery.setFirstResult(0);
		typedQuery.setMaxResults(2);

		List<Categoria> resultList = typedQuery.getResultList();
		assertFalse(resultList.isEmpty());

		resultList.forEach(categoria -> System.out.println(categoria.getNome()));
	}

}
