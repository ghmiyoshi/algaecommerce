package br.com.algaworks.ecommerce.relacionamentos;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Cliente;
import br.com.algaworks.ecommerce.model.Pedido;
import br.com.algaworks.ecommerce.model.StatusPedidoEnum;

public class ListenersTest extends EntityManagerTest {

	@Test
	public void listeners() {
		Cliente cliente = entityManager.find(Cliente.class, 1);
		Pedido pedido = new Pedido();

		pedido.setCliente(cliente);
		pedido.setStatus(StatusPedidoEnum.AGUARDANDO);
		pedido.setTotal(BigDecimal.TEN);

		entityManager.getTransaction().begin();

		entityManager.persist(pedido);
		entityManager.flush();

		pedido.setStatus(StatusPedidoEnum.PAGO);
		entityManager.getTransaction().commit();

		entityManager.clear();

		Pedido pedidoVerificacao = entityManager.find(Pedido.class, pedido.getId());

		Assert.assertNotNull(pedidoVerificacao.getDataCriacao());
		Assert.assertNotNull(pedidoVerificacao.getDataUltimaAtualizacao());
	}

}
