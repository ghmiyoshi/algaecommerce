package br.com.algaworks.ecommerce.iniciandocomjpa;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Produto;

public class OperacoesComTransacaoTest extends EntityManagerTest {

	@Test
	public void inserirOPrimeiroObjeto() {
		Produto produto = new Produto();
		produto.setNome("C�mera Cannon");
		produto.setDescricao("A melhor defini��o para suas fotos");
		produto.setPreco(new BigDecimal(5000.56));

		// N�o precisa estar entre a abertura e fechamento da transa��o, o comando aguarda que tenha a abertura e fechamento da transa��o
		entityManager.persist(produto); 
		entityManager.getTransaction().begin();
		entityManager.getTransaction().commit();
		
		entityManager.clear(); // Limpo a mem�ria do entity manager para executar o select e verificar a asser��o
		
		Produto produtoVerificacao = entityManager.find(Produto.class, produto.getId());

		Assert.assertNotNull(produtoVerificacao);
	}
	
	@Test
	public void removerObjeto() {
		Produto produto = entityManager.find(Produto.class, 3);

		entityManager.getTransaction().begin();
		entityManager.remove(produto);
		entityManager.getTransaction().commit();
		
		// entityManager.clear(); N�o � necess�rio na asser��o para opera��o de remo��o.

		Produto produtoVerificacao = entityManager.find(Produto.class, 3);

		Assert.assertNull(produtoVerificacao);
	}

	@Test
	public void atualizarObjeto() {
		Produto produto = new Produto();
		produto.setId(1);
		produto.setNome("Kindle Paperwhite");
		produto.setDescricao("Eletr�nicos");

		entityManager.getTransaction().begin();
		entityManager.merge(produto);
		entityManager.getTransaction().commit();

		entityManager.clear();

		Produto produtoVerificacao = entityManager.find(Produto.class, produto.getId());

		Assert.assertNotNull(produtoVerificacao);
		Assert.assertEquals("Kindle Paperwhite", produtoVerificacao.getNome());
	}
	
	@Test
	public void atualizarObjetoGerenciado() {
		Produto produto = entityManager.find(Produto.class, 1);

		entityManager.getTransaction().begin();
		produto.setNome("Kindle Paperwhite 2� Gera��o");
		entityManager.getTransaction().commit();

		entityManager.clear();

		Produto produtoVerificacao = entityManager.find(Produto.class, produto.getId());

		Assert.assertNotNull(produtoVerificacao.getDescricao());
		Assert.assertEquals("Kindle Paperwhite 2� Gera��o", produtoVerificacao.getNome());
	}

}
