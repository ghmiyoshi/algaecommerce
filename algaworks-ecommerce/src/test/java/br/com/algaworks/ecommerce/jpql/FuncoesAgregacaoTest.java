package br.com.algaworks.ecommerce.jpql;

import static org.junit.Assert.assertFalse;

import java.util.List;

import javax.persistence.TypedQuery;

import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;

public class FuncoesAgregacaoTest extends EntityManagerTest {

	@Test
	public void aplicarFuncaoAVG() {
		String jpql = "SELECT avg(p.total) FROM Pedido p";

		TypedQuery<Number> typedQuery = entityManager.createQuery(jpql, Number.class);
		List<Number> resultList = typedQuery.getResultList();

		assertFalse(resultList.isEmpty());

		resultList.forEach(number -> System.out.println(number));
	}

	@Test
	public void aplicarFuncaoCOUNT() {
		String jpql = "SELECT count(p.dataCriacao) FROM Pedido p";

		TypedQuery<Number> typedQuery = entityManager.createQuery(jpql, Number.class);
		List<Number> resultList = typedQuery.getResultList();

		assertFalse(resultList.isEmpty());

		resultList.forEach(number -> System.out.println(number));
	}

	@Test
	public void aplicarFuncaoMIN() {
		String jpql = "SELECT min(p.total) FROM Pedido p";

		TypedQuery<Number> typedQuery = entityManager.createQuery(jpql, Number.class);
		List<Number> resultList = typedQuery.getResultList();

		assertFalse(resultList.isEmpty());

		resultList.forEach(number -> System.out.println(number));
	}

	@Test
	public void aplicarFuncaoMAX() {
		String jpql = "SELECT max(p.total) FROM Pedido p";

		TypedQuery<Number> typedQuery = entityManager.createQuery(jpql, Number.class);
		List<Number> resultList = typedQuery.getResultList();

		assertFalse(resultList.isEmpty());

		resultList.forEach(number -> System.out.println(number));
	}

	@Test
	public void aplicarFuncaoSUM() {
		String jpql = "SELECT sum(p.total) FROM Pedido p";

		TypedQuery<Number> typedQuery = entityManager.createQuery(jpql, Number.class);
		List<Number> resultList = typedQuery.getResultList();

		assertFalse(resultList.isEmpty());

		resultList.forEach(number -> System.out.println(number));
	}

}
