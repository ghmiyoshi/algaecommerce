package br.com.algaworks.ecommerce.jpql;

import static org.junit.Assert.assertFalse;

import java.util.List;

import javax.persistence.TypedQuery;

import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;

public class FuncoesColecoesTest extends EntityManagerTest {

	@Test
	public void aplicarFuncaoAvg() {
		String jpql = "SELECT avg(p.total) from Pedido p";

		TypedQuery<Number> typedQuery = entityManager.createQuery(jpql, Number.class);
		List<Number> resultList = typedQuery.getResultList();

		assertFalse(resultList.isEmpty());

		resultList.forEach(obj -> System.out.println(obj));
	}

}
