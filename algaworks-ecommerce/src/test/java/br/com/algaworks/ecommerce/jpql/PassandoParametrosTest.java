package br.com.algaworks.ecommerce.jpql;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.NotaFiscal;
import br.com.algaworks.ecommerce.model.Pedido;
import br.com.algaworks.ecommerce.model.StatusPagamentoEnum;

public class PassandoParametrosTest extends EntityManagerTest {

	@Test
	public void passarParametro() {
		String jpql = "SELECT p FROM Pedido p JOIN p.pagamento pag WHERE p.id = :pedidoId AND pag.status = ?2";

		TypedQuery<Pedido> typedQuery = entityManager.createQuery(jpql, Pedido.class);
		typedQuery.setParameter("pedidoId", 1);
		typedQuery.setParameter(2, StatusPagamentoEnum.PROCESSANDO);

		List<Pedido> resultList = typedQuery.getResultList();

		assertFalse(resultList.isEmpty());
		assertTrue(resultList.size() == 1);
	}
	
	@Test
	public void passarParametroDate() {
		String jpql = "SELECT nf FROM NotaFiscal nf WHERE nf.dataEmissao <= ?1";

		TypedQuery<NotaFiscal> typedQuery = entityManager.createQuery(jpql, NotaFiscal.class);
		typedQuery.setParameter(1, new Date(), TemporalType.TIMESTAMP);

		List<NotaFiscal> resultList = typedQuery.getResultList();

		assertTrue(resultList.size() == 1);
	}

}
