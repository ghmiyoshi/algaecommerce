package br.com.algaworks.ecommerce.mapeamentobasico;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Cliente;
import br.com.algaworks.ecommerce.model.SexoClienteEnum;

public class MapeandoEnumeracoesTest extends EntityManagerTest {

	@Test
	public void mapeamentoEnum() {
		Cliente cliente = new Cliente();
		cliente.setNome("Jos� Mineiro");
		cliente.setCpf("12345678901");
		cliente.setSexo(SexoClienteEnum.MASCULINO);

		entityManager.persist(cliente);

		entityManager.getTransaction().begin();
		entityManager.getTransaction().commit();
		entityManager.clear();

		Cliente clienteVerficacao = entityManager.find(Cliente.class, cliente.getId());
		assertNotNull(clienteVerficacao);
		assertEquals(SexoClienteEnum.MASCULINO, clienteVerficacao.getSexo());
		
	}

}
