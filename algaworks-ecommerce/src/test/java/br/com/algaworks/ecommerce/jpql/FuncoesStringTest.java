package br.com.algaworks.ecommerce.jpql;

import static org.junit.Assert.assertFalse;

import java.util.List;

import javax.persistence.TypedQuery;

import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;

public class FuncoesStringTest extends EntityManagerTest {

	@Test
	public void aplicarFuncaoConcat() {
		String jpql = "SELECT c.nome, CONCAT('Categoria: ', c.nome) FROM Categoria c"; // Concateno a String
		
		TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
		
		resultList.forEach(array -> System.out.println(array[0] + " - " + array[1]));
	}
	
	@Test
	public void aplicarFuncaoLength() {
		String jpql = "SELECT c.nome, length(c.nome) FROM Categoria c"; // Pego tamanho da String
		
		TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
		
		resultList.forEach(array -> System.out.println(array[0] + " - " + array[1]));
	}

	@Test
	public void aplicarFuncaoLengthWhere() {
		String jpql = "SELECT c.nome, length(c.nome) FROM Categoria c WHERE length(c.nome) > 10"; // Pego tamanho da String
		
		TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
		
		resultList.forEach(array -> System.out.println(array[0] + " - " + array[1]));
	}
	
	@Test
	public void aplicarFuncaoLocate() {
		String jpql = "SELECT c.nome, locate('a', c.nome) FROM Categoria c"; // Pego o index da letra a
		
		TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
		
		resultList.forEach(array -> System.out.println(array[0] + " - " + array[1]));
	}
	
	@Test
	public void aplicarFuncaoSubstring() {
		String jpql = "SELECT c.nome, substring(c.nome, 1, 2) FROM Categoria c"; // Pego dois caracteres a partir da posi��o 1
		
		TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
		
		resultList.forEach(array -> System.out.println(array[0] + " - " + array[1]));
	}
	
	@Test
	public void aplicarFuncaoLower() {
		String jpql = "SELECT c.nome, lower(c.nome) FROM Categoria c"; // Deixo a String toda em minuscula
		
		TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
		
		resultList.forEach(array -> System.out.println(array[0] + " - " + array[1]));
	}
	
	@Test
	public void aplicarFuncaoUpper() {
		String jpql = "SELECT c.nome, upper(c.nome) FROM Categoria c"; // Deixo a String toda em maiuscula
		
		TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
		
		resultList.forEach(array -> System.out.println(array[0] + " - " + array[1]));
	}
	
	@Test
	public void aplicarFuncaoTrim() {
		String jpql = "SELECT c.nome, trim(c.nome) FROM Categoria c"; // Removo espa�o do inicio ao fim da String
		
		TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		assertFalse(resultList.isEmpty());
		
		resultList.forEach(array -> System.out.println(array[0] + " - " + array[1]));
	}
	
}
