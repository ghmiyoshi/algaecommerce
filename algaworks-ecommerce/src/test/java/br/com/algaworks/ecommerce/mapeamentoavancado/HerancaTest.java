package br.com.algaworks.ecommerce.mapeamentoavancado;

import java.util.List;

import javax.persistence.TypedQuery;

import org.junit.Assert;
import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Cliente;
import br.com.algaworks.ecommerce.model.Pagamento;
import br.com.algaworks.ecommerce.model.PagamentoCartao;
import br.com.algaworks.ecommerce.model.Pedido;
import br.com.algaworks.ecommerce.model.SexoClienteEnum;
import br.com.algaworks.ecommerce.model.StatusPagamentoEnum;

public class HerancaTest extends EntityManagerTest {
	
	@Test
	public void salvarCliente() {
		Cliente cliente = new Cliente();
		
		cliente.setNome("Fernando");
		cliente.setSexo(SexoClienteEnum.MASCULINO);
		cliente.setCpf("12345678912");
		
		entityManager.getTransaction().begin();
		entityManager.persist(cliente);
		entityManager.getTransaction().commit();
		
		entityManager.clear();
		
		Cliente clienteVerificacao = entityManager.find(Cliente.class, cliente.getId());
		
		Assert.assertNotNull(clienteVerificacao);
		Assert.assertNotNull(clienteVerificacao.getId());
	}
	
	@Test
	public void atualizarCliente() {
		Cliente cliente = entityManager.find(Cliente.class, 3);
		
		cliente.setNome("Teste");
		cliente.setSexo(SexoClienteEnum.MASCULINO);
		
		entityManager.getTransaction().begin();
		entityManager.persist(cliente);
		entityManager.getTransaction().commit();
		
		entityManager.clear();
		
		Cliente clienteVerificacao = entityManager.find(Cliente.class, cliente.getId());
		
		Assert.assertNotNull(clienteVerificacao);
		Assert.assertNotNull(clienteVerificacao.getId());
	}
	
	@Test
    public void buscarPagamentos() {
		TypedQuery<Pagamento> typedQuery = entityManager.createQuery("select p from Pagamento p", Pagamento.class);
		List<Pagamento> pagamentos = typedQuery.getResultList();

        Assert.assertFalse(pagamentos.isEmpty());
    }

    @Test
    public void incluirPagamentoPedido() {
        Pedido pedido = entityManager.find(Pedido.class, 1);

        PagamentoCartao pagamentoCartao = new PagamentoCartao();
        pagamentoCartao.setPedido(pedido);
        pagamentoCartao.setStatus(StatusPagamentoEnum.PROCESSANDO);
        pagamentoCartao.setNumeroCartao("123");

        entityManager.getTransaction().begin();
        entityManager.persist(pagamentoCartao);
        entityManager.getTransaction().commit();

        entityManager.clear();

        Pedido pedidoVerificacao = entityManager.find(Pedido.class, pedido.getId());
        Assert.assertNotNull(pedidoVerificacao.getPagamento());
    }

}
