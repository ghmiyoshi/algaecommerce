package br.com.algaworks.ecommerce.jpql;

import static org.junit.Assert.assertFalse;

import java.util.List;

import javax.persistence.TypedQuery;

import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;

public class AgrupandoRegistrosTest extends EntityManagerTest {

	@Test
	public void agruparResultadoCount() {
		// Quantidade de produtos por categoria
		String jpql = "SELECT c.nome, count(p.id) FROM Categoria c JOIN c.produtos p GROUP BY c.id";

		TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		resultList.forEach(arr -> System.out.println(arr[0] + " - " + arr[1]));

		assertFalse(resultList.isEmpty());
	}
	
	@Test
	public void agruparResultadoCount2() {
		// Total de vendas por m�s
		String jpql = "SELECT concat(year(p.dataCriacao), '/', function('monthname', p.dataCriacao)), SUM(p.total) FROM Pedido p GROUP BY year(p.dataCriacao), month(p.dataCriacao)";

		TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		resultList.forEach(arr -> System.out.println(arr[0] + " - " + arr[1]));

		assertFalse(resultList.isEmpty());
	}
	
	@Test
	public void agruparResultadoCount3() {
		// Total de vendas por categoria
		String jpql = "select c.nome, sum(ip.precoProduto * ip.quantidade) " +
		   "from ItemPedido ip join ip.produto pro join pro.categorias c " +
		   "group by c.id";
		
		TypedQuery<Object[]> typedQuery = entityManager.createQuery(jpql, Object[].class);
		List<Object[]> resultList = typedQuery.getResultList();
		
		resultList.forEach(arr -> System.out.println(arr[0] + " - " + arr[1]));
 
		assertFalse(resultList.isEmpty());
	}

}
