package br.com.algaworks.ecommerce.iniciandocomjpa;

import org.junit.Assert;
import org.junit.Test;

import br.com.algaworks.ecommerce.EntityManagerTest;
import br.com.algaworks.ecommerce.model.Produto;

public class ConsultandoRegistrosTest extends EntityManagerTest {
	
	@Test
	public void buscaPorIdentificador() {
		Produto produto = entityManager.find(Produto.class, 1);
		Produto produtoRef = entityManager.getReference(Produto.class, 1); // Busca na base somente quando uso alguma propriedade do objeto ex: produtoRef.getNome()

		Assert.assertNotNull(produtoRef);
		Assert.assertEquals("Kindle", produto.getNome());
	}
	
	@Test
	public void atualizarReferencia() {
		Produto produto = entityManager.find(Produto.class, 1);
		produto.setNome("Microfone JBL");

		entityManager.refresh(produto); // Volto o estado da base, busco o objeto de novo

		Assert.assertEquals("Kindle", produto.getNome());
	}

}
